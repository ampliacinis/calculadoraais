﻿/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

    import java.awt.*;
    import javax.swing.*;
    import java.awt.event.*;
    import java.awt.BorderLayout;
    import java.awt.GridLayout;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.script.ScriptException;

    //no son necesarios, se incluye en javax.swing.*
    import javax.swing.JFrame;
    import javax.swing.JTextField;
    import javax.swing.JButton;
    import javax.swing.JPanel;    

public class Interfaces {
   private Boolean operador=false;
   private Boolean operadoR=true;
   private int parA=0;
   private int parC=0;
   private Boolean number=true;
   private datos calculadora;
   private JTextField texto;
   private JPanel panelTexto, panelBotones,panelBotones2;
   private JButton boton1, boton2, boton3, boton4, boton5, boton6, boton7, boton8, boton9, boton0,botonD;
   private JButton botonc,botonDecimal,botonPosneg,botonResta, botonSuma, botonMultiplicacion, botonDivision, botonIgual,botonParentesisA,botonParentesisC ;
   private JFrame frame;
   private Container panel;
   BorderLayout borderLayout1 = new BorderLayout();
   GridLayout gridLayout1 = new GridLayout();
   GridLayout gridLayout2 = new GridLayout();
      //Variables  
   String valor;

    
    private void botonDecimalActionPerformed(java.awt.event.ActionEvent evt) {
        if (texto.getText().contains("ERROR")){
            botonc.doClick();
            operador=false;
        }
         
        if (texto.getText().equals("") ){
            texto.setText("0.");
            botonDecimal.setEnabled(false);
        }
        else if (!operador){
            texto.setText(texto.getText()+"0.");
            operador=false;
            botonDecimal.setEnabled(false);
        }
        else        
            texto.setText(texto.getText()+botonDecimal.getText());
        //Desactivamos los operadores 
            botonSuma.setEnabled(false);
            botonResta.setEnabled(false);
            botonDivision.setEnabled(false);
            botonMultiplicacion.setEnabled(false);
            botonParentesisA.setEnabled(false);
            botonParentesisC.setEnabled(false);
            botonDecimal.setEnabled(false);
        panel.requestFocus();
    }                                       

    private void botoncActionPerformed(java.awt.event.ActionEvent evt) {
        /*if (texto.getText().contains("ERROR"))
            texto.setText("");*/
        texto.setText("");
        //Activamos los operadores y numeros
        botonSuma.setEnabled(true);
        botonResta.setEnabled(true);
        //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
        botonParentesisA.setEnabled(true);
        botonParentesisC.setEnabled(true);
        number= true;
        operador=false;
        parA=0;
        parC=0;
        panel.requestFocus();
        botonDecimal.setEnabled(true);
    }

    private void botonDActionPerformed(java.awt.event.ActionEvent evt) { 
        if (texto.getText().contains("ERROR"))
           botonc.doClick();
        String text =texto.getText();
        
        if (text.length()>1){
            texto.setText( text.substring(0, text.length()-1));
            char a=text.charAt(text.length()-2);
            if (a=='1' || a=='2' || a=='3' || a=='4' || a=='5' || a=='6' || a=='7' || a=='8' || a=='9' || a=='0' || a==')'){
               operador=true;
               operadoR=true;
            }else if (a=='-'){
             operadoR=false;
             operador=false;
            }else{
             operadoR=true;
             operador=false;
            }
            //Activamos los operadores 
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
            botonDecimal.setEnabled(true);
            operador=false;
        }
        else{
            texto.setText("");
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(false);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(false);
            botonMultiplicacion.setEnabled(false);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(false);
            operador=false;
        }
        panel.requestFocus();
    }

    private void boton1ActionPerformed(java.awt.event.ActionEvent evt) {   
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
            //texto.setText("");
        if (number){    //Para controlar que despues de un ")" no se pueda meter un numero
            texto.setText(texto.getText()+boton1.getText());
            panel.requestFocus();
            operador=true;
            operadoR=true;
            number=true;
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
        }
    }     
  
    private void boton2ActionPerformed(java.awt.event.ActionEvent evt) { 
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
        if (number){    //Para controlar que despues de un ) no se pueda meter un numero
            texto.setText(texto.getText()+boton2.getText());  
            panel.requestFocus();
            operador=true;
            operadoR=true;
            number=true;
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
        }
     }
    
    private void boton3ActionPerformed(java.awt.event.ActionEvent evt) {    
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
        if (number){    //Para controlar que despues de un ) no se pueda meter un numero
            texto.setText(texto.getText()+boton3.getText());
            panel.requestFocus();
            operador=true;
            operadoR=true;
            number=true;
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
        }
    }

    private void boton4ActionPerformed(java.awt.event.ActionEvent evt) { 
        if (texto.getText().contains("ERROR"))
            texto.setText("");
        if (number){    //Para controlar que despues de un ) no se pueda meter un numero
            texto.setText(texto.getText()+boton4.getText());
            panel.requestFocus();
            operador=true;
            operadoR=true;
            number=true;
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
        }
    }
      
    private void boton5ActionPerformed(java.awt.event.ActionEvent evt) { 
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
        if (number){    //Para controlar que despues de un ) no se pueda meter un numero
            texto.setText(texto.getText()+boton5.getText());
            panel.requestFocus();
            operador=true;
            operadoR=true;
            number=true;
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
        }
    }
         
    private void boton6ActionPerformed(java.awt.event.ActionEvent evt) { 
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
        if (number){    //Para controlar que despues de un ) no se pueda meter un numero
            texto.setText(texto.getText()+boton6.getText());
            panel.requestFocus();
            operador=true;
            operadoR=true;
            number=true;
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
        }
    }
      
    private void boton7ActionPerformed(java.awt.event.ActionEvent evt) { 
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
        if (number){    //Para controlar que despues de un ) no se pueda meter un numero
            texto.setText(texto.getText()+boton7.getText());
            panel.requestFocus();
            operador=true;
            operadoR=true;
            number=true;
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
        }
    }

    private void boton8ActionPerformed(java.awt.event.ActionEvent evt) {   
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
        if (number){    //Para controlar que despues de un ) no se pueda meter un numero
            texto.setText(texto.getText()+boton8.getText());
            panel.requestFocus();
            operador=true;
            operadoR=true;
            number=true;
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
        }
    }
   
     private void boton9ActionPerformed(java.awt.event.ActionEvent evt) { 
        if (texto.getText().contains("ERROR"))
           botonc.doClick();
        if (number){    //Para controlar que despues de un ) no se pueda meter un numero
            texto.setText(texto.getText()+boton9.getText());
            panel.requestFocus();
            operador=true;
            operadoR=true;
            number=true;
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
        }
     }
    private void boton0ActionPerformed(java.awt.event.ActionEvent evt) {  
        if (texto.getText().contains("ERROR"))
           botonc.doClick();
        if (number){    //Para controlar que despues de un ) no se pueda meter un numero
            texto.setText(texto.getText()+boton0.getText());
            panel.requestFocus();
            operador=true;
            operadoR=true;
            number=true;
            //Activamos los operadores tras introducir un numero
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Si se ha introducido antes un decimal, se deshabilitan los parentesis, ahora los activamos de nuevo
            botonParentesisA.setEnabled(true);
            botonParentesisC.setEnabled(true);
        }
     }
     
    private  void botonSumaActionPerformed(java.awt.event.ActionEvent evt) {
        if (texto.getText().contains("ERROR"))
           botonc.doClick();
        if(operador){
            valor =texto.getText();
            // datos.Sumar(valor);
            texto.setText( valor + "+" );
            panel.requestFocus();
            operador=false;
            operadoR=false;
            number=true;
            //Desactivamos cualquier operador para no permitir la introduccion de 2 operadores seguidos
            botonSuma.setEnabled(false);
            botonResta.setEnabled(false);
            botonDivision.setEnabled(false);
            botonMultiplicacion.setEnabled(false);
            botonDecimal.setEnabled(true);
        }/*
        else if (texto.getText().equals("")){
             texto.setText( "+" );
             operador=false;
             //Desactivamos cualquier operador para no permitir la introduccion de 2 operadores seguidos
            botonSuma.setEnabled(false);
            botonResta.setEnabled(false);
            botonDivision.setEnabled(false);
            botonMultiplicacion.setEnabled(false);
        }*/
    }   
    
    private void botonRestaActionPerformed(java.awt.event.ActionEvent evt) {
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
        if (operadoR){
            valor =texto.getText();
            // datos.Resta(valor);  
            texto.setText( valor + "-");
            panel.requestFocus();
            operador=false;
            operadoR=false;
            number=true;
            //Desactivamos cualquier operador para no permitir la introduccion de 2 operadores seguidos
            botonSuma.setEnabled(false);
            botonResta.setEnabled(false);
            botonDivision.setEnabled(false);
            botonMultiplicacion.setEnabled(false);
            botonDecimal.setEnabled(true);
        }
        else if (texto.getText().equals("")){
             texto.setText( "-" );
             operador=false;
             //Desactivamos cualquier operador para no permitir la introduccion de 2 operadores seguidos
            botonSuma.setEnabled(false);
            botonResta.setEnabled(false);
            botonDivision.setEnabled(false);
            botonMultiplicacion.setEnabled(false);
            botonDecimal.setEnabled(true);
        }
    }
   
    private void botonMultiplicacionActionPerformed(java.awt.event.ActionEvent evt) {  
        if (texto.getText().contains("ERROR"))
           botonc.doClick();
        if(operador){
            valor =texto.getText();
            //  datos.Multiplicacion(valor);
            texto.setText( valor + "*");
            operador=false;
            number=true;
            //Desactivamos cualquier operador para no permitir la introduccion de 2 operadores seguidos
            botonSuma.setEnabled(false);
            botonResta.setEnabled(false);
            botonDivision.setEnabled(false);
            botonMultiplicacion.setEnabled(false);
            botonDecimal.setEnabled(true);
        }
     }
     
    private void botonDivisionActionPerformed(java.awt.event.ActionEvent evt) {  
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
        if(operador){
            valor =texto.getText();
            //datos.Division(valor);
            texto.setText( valor + "/");
            operador=false;
            number=true;
            //Desactivamos cualquier operador para no permitir la introduccion de 2 operadores seguidos
            botonSuma.setEnabled(false);
            botonResta.setEnabled(false);
            botonDivision.setEnabled(false);
            botonMultiplicacion.setEnabled(false);
            botonDecimal.setEnabled(true);
        }
    }
    
    private void botonParentesisAActionPerformed(java.awt.event.ActionEvent evt) {
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
        if( ! operador){
            valor =texto.getText();
            // datos.Division(valor);
            texto.setText( valor + "(");
            panel.requestFocus();
            operador=false;
            operadoR=true;
            number=true;
            //Activamos los operadores "-" tras introducir un numero
            botonSuma.setEnabled(false);
            botonResta.setEnabled(true);
            //Actualizamos el contador de parentesis abiertos
            parA=parA+1;
            botonDecimal.setEnabled(true);
        }
    }
        
    private void botonParentesisCActionPerformed(java.awt.event.ActionEvent evt) {   
        if (texto.getText().contains("ERROR"))
            botonc.doClick();
        if(operador && parA>parC){
            valor =texto.getText();
            //datos.Division(valor);
            texto.setText( valor + ")");
            panel.requestFocus();
            operador=true;
            number=false;
            //Activamos los operadores tras introducir un parentesis cerrado
            botonSuma.setEnabled(true);
            botonResta.setEnabled(true);
            botonDivision.setEnabled(true);
            botonMultiplicacion.setEnabled(true);
            //Actualizamos el contador de parentesis cerrados
            parC= parC+1;
        }
    }
   
   
     
    private void igualActionPerformed(java.awt.event.ActionEvent evt) throws ScriptException {                                      
      if (parA==parC){   //Aseguramos que haya el mismo numero de parentesis abiertos y cerrados
        valor=texto.getText();       
        String resul= String.valueOf(datos.igual(valor));
        if (resul.equals("Infinity"))
          texto.setText("ERROR: No se puede dividir por 0.");
        else if (resul.equals("null")){
            //Si se ha introducido una cadena como 3*= devolverá null, por tanto no realizamos ninguna accion
        }else
          texto.setText(resul);
        panel.requestFocus();
      }
    }

   public Interfaces(datos calculadora) {  
    this.calculadora = calculadora;   
    frame =new JFrame();
    frame.setLayout( borderLayout1);
    panel = frame.getContentPane();    
 
    texto = new JTextField("");
    texto.setEnabled(false);
    texto.setBorder(BorderFactory.createLineBorder(Color.blue));
    panel.add(texto, BorderLayout.NORTH);
    texto.setPreferredSize(new Dimension(50,50));
    texto.setHorizontalAlignment(JTextField.RIGHT); 
    texto.setDisabledTextColor(Color.white); 
    texto.setBackground(Color.black);
    
    
    botonD = new JButton("DEL");
    boton1 = new JButton("1");  
    boton2 = new JButton("2");
    boton3 = new JButton("3");
    boton4 = new JButton("4");
    boton5 = new JButton("5");
    boton6 = new JButton("6");
    boton7 = new JButton("7");
    boton8 = new JButton("8");
    boton9 = new JButton("9");
    botonSuma = new JButton("+");
    botonResta = new JButton("-");
    botonMultiplicacion = new JButton("*");
    botonDivision = new JButton("/");
    botonIgual = new JButton("=");
    boton0 = new JButton("0");
    botonc = new JButton("C");
    panelBotones = new JPanel();
    botonDecimal = new JButton(".");
    botonPosneg = new JButton("+/-");
    botonParentesisA = new JButton("(");
    botonParentesisC = new JButton(")");
         
    panelBotones.setLayout(gridLayout2);
    
    panelBotones.add(boton7);
    panelBotones.add(boton8);
    panelBotones.add(boton9);
    panelBotones.add(botonD); 
    panelBotones.add(botonc);    
    panelBotones.add(boton4);
    panelBotones.add(boton5);
    panelBotones.add(boton6);    
    panelBotones.add(botonMultiplicacion);
    panelBotones.add(botonDivision);
    panelBotones.add(boton1);
    panelBotones.add(boton2);
    panelBotones.add(boton3);    
    panelBotones.add(botonSuma);
    panelBotones.add(botonResta);
    panelBotones.add(boton0);    
    panelBotones.add(botonDecimal);    
    panelBotones.add(botonParentesisA);  
    panelBotones.add(botonParentesisC);
    panelBotones.add(botonIgual);

   
    borderLayout1.setVgap(10);
    borderLayout1.setHgap(10);
    botonc.setPreferredSize(new Dimension(75,75));
    botonIgual.setPreferredSize(new Dimension(75,75));


    panel.add(panelBotones, BorderLayout.CENTER);
    
    gridLayout2.setHgap(10);
  
    gridLayout2.setVgap(10);
    gridLayout1.setHgap(10);
    gridLayout2.setColumns(4);
    gridLayout2.setRows(4);
    gridLayout1.setVgap(10);
    
    frame.setSize(500,500);
    frame.setTitle("CALCULADORA");
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);    
    frame.setVisible(true);
    frame.setFocusable (true);
    
    
    boton1.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            boton1ActionPerformed(e);
        }
    });
    boton2.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            boton2ActionPerformed(e);
        }
    });
    boton3.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            boton3ActionPerformed(e);
            
        }
    });
    boton4.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            boton4ActionPerformed(e);
        }
    });
    
      boton5.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            boton5ActionPerformed(e);
        }
    });
          
    boton6.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            boton6ActionPerformed(e);
        }
    });
    
    boton7.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            boton7ActionPerformed(e);
        }
    });

    boton8.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            boton8ActionPerformed(e);
        }
    });
    
    boton9.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            boton9ActionPerformed(e);
        }
    });
    boton0.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            boton0ActionPerformed(e);
        }
    });

    botonc.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            botoncActionPerformed(e);
        }
    });
    
    botonSuma.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            botonSumaActionPerformed(e);
        }
    });
    botonResta.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            botonRestaActionPerformed(e);
        }
    });
    botonDivision.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            botonDivisionActionPerformed(e);
        }
    });
    botonMultiplicacion.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            botonMultiplicacionActionPerformed(e);
        }
    });
    botonDecimal.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            botonDecimalActionPerformed(e);
        }
    });
    botonIgual.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            try {
                igualActionPerformed(e);
            } catch (ScriptException ex) {
                Logger.getLogger(Interfaces.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    });
        botonParentesisA.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            botonParentesisAActionPerformed(e);
        }
    });
        
       botonParentesisC.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            botonParentesisCActionPerformed(e);
        }
    });
        botonD.addActionListener(new ActionListener(){
        public void actionPerformed (ActionEvent e){
            botonDActionPerformed(e);
        }
    });
    
    
    panel.addKeyListener( new KeyListener(){
        public void keyPressed( KeyEvent e ){
                int tecla=e.getKeyChar();
                if (tecla==48 || tecla==96)
                    boton0.doClick();
                else if (tecla==10)
                    botonIgual.doClick();
                else if (tecla==127)
                    botonc.doClick();
                else if (tecla==8)
                    botonD.doClick();
                else if (tecla==49)
                    boton1.doClick();
                else if (tecla==50)
                    boton2.doClick();
                else if (tecla==51)
                    boton3.doClick();
                else if (tecla==52)
                    boton4.doClick();
                else if (tecla==53)
                    boton5.doClick();
                else if (tecla==54)
                    boton6.doClick();
                else if (tecla==55)
                    boton7.doClick();
                else if (tecla==56)
                    boton8.doClick();
                else if (tecla==57)
                    boton9.doClick();
                else if (tecla==42)
                    botonMultiplicacion.doClick();
                else if (tecla==43)
                    botonSuma.doClick();
                else if (tecla==45)
                    botonResta.doClick();
                else if (tecla==46)
                    botonDecimal.doClick();
                else if (tecla==47)
                    botonDivision.doClick();
                else if (tecla==40)
                    botonParentesisA.doClick();
                else if (tecla==41)
                    botonParentesisC.doClick();
                else if(tecla==65535 || tecla==15){
                    //Para tecla shift
                }else
                    texto.setText("ERROR: El caracter introducido no es válido.");
     		}
                

       
        public void keyTyped(KeyEvent e) {
           
        }

       
        public void keyReleased(KeyEvent e) {
                        
      

        }
     		}); 
     panel.requestFocus();
   }
  
}
